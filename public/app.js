const appConfig = {

	mounted: function() {
		this.initializeBoard();
		this.drawBoard();
	},

	data() {
		return {
			board: {
				sizeX: 32,
				sizeY: 32,
				aliveSurvive: 23,
				dedResurrect: 3,
				generationThreshold: 0.5
			},
			boardArray: null,
			modified: false,
			currentIteration: 0,
			maxIterations: 1,
			increment: 50,
			pixelSize: 14,
		}
	},
	
	computed: {
		canvasSizeX() {
			return this.board.sizeX * this.pixelSize;
		},

		canvasSizeY() {
			return this.board.sizeY * this.pixelSize;
		},
	},

	watch: {
		board: {
			// whenever board changes, this function will run
			handler: async function () {
				await this.$nextTick();
				this.modified = true;
				this.currentIteration = 0;
				this.drawBoard();
			},
			deep: true
		},

		currentIteration: function () {
			this.drawBoard();
		}
	},

	methods: {
		countAliveNeighbours(board, xPos, yPos) {
			const sizeX = this.board.sizeX;
			const sizeY = this.board.sizeY;

			const dist = 1
			let count = 0;
			for(let x = xPos - dist; x <= xPos + dist; x++) {
				let x2 = x;
				if(x2 < 0) x2 += sizeX;
				else if(x2 >= sizeX) x2 -= sizeX;

				for(let y = yPos - dist; y <= yPos + dist; y++) {
					let y2 = y;
					if(y2 < 0) y2 += sizeY;
					else if(y2 >= sizeY) y2 -= sizeY;
					
					if(x2 != xPos || y2 != yPos) count += board[x2][y2];
				}
			}
			return count;
		},

		calculateNextBoard(currentBoard) {
			const sizeX = this.board.sizeX;
			const sizeY = this.board.sizeY;

			const aliveSurvive = this.board.aliveSurvive.toString().split('').map(Number);
			const dedResurrect = this.board.dedResurrect.toString().split('').map(Number);

			const nextBoard = Array.from(Array(sizeX), () => new Array(sizeY));

			for(let x = 0; x < sizeX; x++) {
				for(let y = 0; y < sizeY; y++) {
					const aliveNeighbours = this.countAliveNeighbours(currentBoard, x, y);

					if (currentBoard[x][y] === 1 && aliveSurvive.includes(aliveNeighbours)) {
						nextBoard[x][y] = 1;
					} else if(currentBoard[x][y] === 0 && dedResurrect.includes(aliveNeighbours)) {
						nextBoard[x][y] = 1;
					} else {
						nextBoard[x][y] = 0;
					}
				}
			}

			return nextBoard;
		},

		generateBoardIncrement() {
			const start = this.maxIterations - 1;
			const board = this.boardArray;

			let currentBoard = board[start];
			for(let i = 0; i < this.increment; i++) {
				const nextBoard = this.calculateNextBoard(currentBoard);
				board.push(nextBoard);
				currentBoard = nextBoard;
			}
			this.maxIterations = this.maxIterations + this.increment;

			this.updateGraph()
		},

		initializeBoard() {
			const sizeX = this.board.sizeX;
			const sizeY = this.board.sizeY;
			this.maxIterations = 1;
			this.currentIteration = 0;

			this.boardArray = Array();
			let currentBoard = Array.from(Array(sizeX), () => new Array(sizeY));
			// initialize board
			for(let x = 0; x < sizeX; x++) {
				for(let y = 0; y < sizeY; y++) {
					if (Math.random() < this.board.generationThreshold) {
						currentBoard[x][y] = 1;
					} else {
						currentBoard[x][y] = 0;
					}
				}
			}
			
			this.boardArray.push(currentBoard);
			this.modified = false;

			this.generateBoardIncrement();
		},

		drawBoard() {
			const sizeX = this.board.sizeX;
			const sizeY = this.board.sizeY;
			const currentIteration = this.currentIteration;

			const currentBoard = this.boardArray[currentIteration];

			const ctx = document.getElementById('boardCanvas').getContext('2d');
			ctx.fillStyle = "black";

			ctx.clearRect(0, 0, this.pixelSize * sizeX, this.pixelSize * sizeY);

			if(!this.modified) {
				for(let x = 0; x < sizeX; x++) {
					for(let y = 0; y < sizeY; y++) {
						if(currentBoard[x][y] == 1) {
							ctx.fillRect(x * this.pixelSize, y * this.pixelSize, this.pixelSize, this.pixelSize);
						}
					}
				}
			}

			return;
		},

		updateGraph() {
			// ded
			chartConfig.data.datasets[0].data.length = 0;
			// alive
			chartConfig.data.datasets[1].data.length = 0;


			const sizeX = this.board.sizeX;
			const sizeY = this.board.sizeY;
			const area = sizeX * sizeY;
			
			const offset = Math.ceil(this.maxIterations / 50);

			for(let t = 0; t < this.maxIterations; t += offset) {
				let alive = 0;
				for(let x = 0; x < sizeX; x++) {
					for(let y = 0; y < sizeY; y++) {
						if (this.boardArray[t][x][y] === 1) alive++;
					}
				}
				
				const ded = area - alive;
				chartConfig.data.datasets[0].data.push({x: t, y: ded / area});
				chartConfig.data.datasets[1].data.push({x: t, y: alive / area});

			}

			new Chart(document.getElementById('myChart').getContext('2d'), chartConfig).update();
		}
	}

};

Vue.createApp(appConfig).mount("#app");