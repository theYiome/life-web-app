const chartConfig = {
    type: 'line',
    data: {
        datasets: [
            {
                label: "Martwe",
                backgroundColor: "lightskyblue",
                borderColor: "lightskyblue",
                // borderWidth: 6,
                // pointRadius: 16,
                // pointHoverRadius: 16,
                data: [
                    {x: 0, y: 3}, 
                    {x: 1, y: 4}, 
                    {x: 2, y: 3}, 
                    {x: 3, y: 4}, 
                    {x: 4, y: 5}
                ],
                fill: false
            },
            {
                label: "Żywe",
                backgroundColor: "black",
                borderColor: "black",
                // borderWidth: 6,
                // pointRadius: 16,
                // pointHoverRadius: 16,
                data: [
                    {x: 0, y: 5}, 
                    {x: 1, y: -6}, 
                    {x: 2, y: 10}, 
                    {x: 3, y: -4},
                    {x: 4, y: 0},
                    {x: 5, y: 8},
                    {x: 6, y: 3}
                ],
                fill: false
            }
        ]
    },
    options: {
        scales: {
            xAxes: [
                {
                    type: 'linear'
                },
                {
                    scaleLabel: {
                        display: true,
                        labelString: "Numer iteracji"
                    }
                }
            ],
            yAxes: [
                {
                    scaleLabel: {
                        display: true,
                        labelString: "Stosunek komórek o danym stanie"
                    }
                }
            ]
        },
        title: {
            display: true,
            text: "Zmiana ilości komórek w czasie"
        },
        animation: {
            duration: 0 // general animation time
        },
        hover: {
            animationDuration: 0 // duration of animations when hovering an item
        },
        responsiveAnimationDuration: 0 // animation duration after a resize
    }
};